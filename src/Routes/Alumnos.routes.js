import { Router } from "express";
import { getAlumno, saveAlumno, updateAlumno, deleteAlumno} from "../Controllers/AlumnosController.js"
import { subirImagen } from "../Middleware/Storage.js"
import { verificar } from '../Middleware/Auth.js'

const rutas = Router()

rutas.get('/api/alumnos',verificar,getAlumno)
rutas.get('/api/alumnos',getAlumno)
rutas.get('/api/alumnos/:id',getAlumno)
rutas.post('/api/alumnos/', subirImagen.single('foto'), saveAlumno)
rutas.put('/api/alumnos/:id', subirImagen.single('foto'), updateAlumno)
rutas.delete('/api/alumnos/:id', deleteAlumno)

export default rutas