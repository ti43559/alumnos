import mongoose from "mongoose";
import * as fs from 'fs';


const esquema = new mongoose.Schema({
    escuela: String, matricula: String, alumno: String, carrera: String, semestre: String, fecha_nacimiento: Date, foto: String
}, {versionKey:false})
const AlumnoModel = mongoose.model('alumnos', esquema)

export const getAlumno = async(req,res) =>{
    try{
        const{id} = req.params
        const rows =
        (id === undefined ) ? await AlumnoModel.find() : await AlumnoModel.findById(id)
        return res.status(200).json({status:true,data:rows})
    }
    catch(error){
        return res.status(500).json({status:false,errors:[error]})
    }
}

export const saveAlumno = async(req,res) =>{
    try{
        const {escuela, matricula, alumno, carrera, semestre, fecha_nacimiento} = req.body
        const validacion = validar(escuela, matricula, alumno, carrera, semestre, fecha_nacimiento, req.file, 'Y')
        if(validacion == ''){
            const nuevoAlumno = new AlumnoModel({
                escuela:escuela,matricula:matricula,alumno:alumno,carrera:carrera,semestre:semestre,fecha_nacimiento:fecha_nacimiento,
                foto:'/uploads/'+req.file.filename
            })
            return await nuevoAlumno.save().then(
                () => {res.status(200).json({status:true,message:'Juego agregado'})}
            )
        }
        else{
            return res.status(400).json({status:false,errors:validacion})
        }
    }
    catch(error){
        return res.status(500).json({status:false,errors:[error.message]})
    }
}


export const updateAlumno = async(req,res) =>{
    try{
        const {id} = req.params
        const {escuela, matricula, alumno, carrera, semestre, fecha_nacimiento} = req.body
        let foto = ''
        let valores = {escuela:escuela,matricula:matricula,alumno:alumno,carrera:carrera,semestre:semestre,fecha_nacimiento:fecha_nacimiento}
        if(req.file != null){
            foto = '/uploads/'+req.file.filename
            valores = {escuela:escuela,matricula:matricula,alumno:alumno,carrera:carrera,semestre:semestre,fecha_nacimiento:fecha_nacimiento,foto:foto}
            await eliminarImagen(id)
        }
        const validacion = validar(escuela, matricula, alumno, carrera, semestre, fecha_nacimiento)
        if(validacion == ''){
            await AlumnoModel.updateOne({_id:id},{$set: valores})
            res.status(200).json({status:true,message:'Juego actualizado'})
        }
        else{
            return res.status(400).json({status:false,errors:validacion})
        }
    }
    catch(error){
        return res.status(500).json({status:false,errors:[error.message]})
    }
}

export const deleteAlumno = async(req,res) =>{
    try{
        const{id} = req.params
        await eliminarImagen(id)
        await AlumnoModel.deleteOne({_id:id})
        return res.status(200).json({status:true,message:'Alumno eliminado'})
    }
    catch(error){
        return res.status(500).json({status:false,errors:[error.message]})
    }
}

const eliminarImagen = async(id) =>{
    const alumno = await AlumnoModel.findById(id)
    const img = alumno.foto
    fs.unlinkSync('./public/'+img)
}

const validar = (escuela, matricula, alumno, carrera, semestre, fecha_nacimiento, img, sevalida) => {
    var errors = []
    if (escuela === undefined || escuela.trim() === '' || isNaN(escuela)){
        errors.push('La escuela no debe estar vacía');
    }
    if (matricula === undefined || matricula.trim() === '' || isNaN(escuela)){
        errors.push('La matrícula no debe estar vacía');
    }
    if (alumno === undefined || alumno.trim() === '' || isNaN(escuela)){
        errors.push('El nombre del alumno no debe estar vacío');
    }
    if (carrera === undefined || carrera.trim() === '' || isNaN(escuela)){
        errors.push('La carrera no debe estar vacía');
    }
    if (semestre === undefined || semestre.trim() === '' || isNaN(escuela)){
        errors.push('El semestre no debe estar vacío');
    }
    if (fecha_nacimiento === undefined || fecha_nacimiento.trim() === '' || isNaN(Date.parse(fecha_nacimiento))) {
        errors.push('La fecha de nacimiento no debe estar vacía y debe ser válida');
    }
    if (sevalida === 'Y' && img === undefined) {
        errors.push('Selecciona una imagen jpg, jpeg o png');
    } 
    else{
        if(errors != ''){
            fs.unlinkSync('./public/uploads/' + img.filename);
        }
    }
    return errors;
}
